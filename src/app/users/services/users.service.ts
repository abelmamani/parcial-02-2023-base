import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import { UserResponse } from '../interfaces/user.response.model';
import { UserRequest } from '../interfaces/user.request.model';
import { UsersResponse } from '../interfaces/users.response.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  url: string = "https://reqres.in/api/users";
  constructor(private httpClient: HttpClient) {
  }

  createUser(userRequest: UserRequest):Observable<UserResponse>{
    return this.httpClient.post<UserResponse>(this.url, userRequest);
  }

  getUsers(): Observable<UsersResponse>{
    return this.httpClient.get<UsersResponse>(this.url+"?page=1");
  }

}
