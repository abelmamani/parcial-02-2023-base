import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UsersService} from "../../services/users.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import { User } from '../../interfaces/user.model';
import { UsersResponse } from '../../interfaces/users.response.model';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
  users: User[] = [];
  constructor(private router: Router,
              private userService: UsersService,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.getUsers();
  }


  addData() {
    this.router.navigate(['/users/user']);
  }

  getUsers(){
    this.userService.getUsers().subscribe({
      next: (res: UsersResponse) => {
        this.users = res.data;
      },
      error: (err) => {
        this.showSnackBar("Ocurrio un error al obtener el listado de users");
      }
    });
  }
  
  showSnackBar(msg: string){
    this.matSnackBar.open(msg, "Cerrar", {duration: 3000});
  }
}
