import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {UsersService} from "../../services/users.service";
import {Router} from "@angular/router";
import { UserResponse } from '../../interfaces/user.response.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    name: [undefined, [Validators.required, Validators.minLength(3)]],
    job: [undefined, [Validators.required, Validators.minLength(3)]],
  });

  constructor(private fb: FormBuilder,
              private matSnackBar: MatSnackBar,
              private userService: UsersService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.miFormulario.reset({});
    this.miFormulario.untouched;
  }

  guardar() {
    if(this.miFormulario.valid){
      this.userService.createUser(this.miFormulario.value).subscribe({
        next: (res: UserResponse) => {
          this.showSnackBar("se registro usuario " + res.id+" "+ res.name + " " +res.job+ " " + res.createdAt);
          this.router.navigate(['/users/listado']);
        },
        error: (err) => {
          this.showSnackBar(err.error ? err.error.message : "ocurrio un problema al registrar el usuario");
        }
      });
    }else{
      this.miFormulario.touched;
    }
  }

  cancelar() {
    this.showSnackBar("se cancelo el registro de usuario");
    this.router.navigate(['/users/listado']);
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje, "Cerrar", {duration: 6000});
  }
}
