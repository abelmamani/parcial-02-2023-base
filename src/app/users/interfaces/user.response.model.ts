export interface UserResponse{
    name: string,
    job: string,
    id: number,
    createdAt: Date
}