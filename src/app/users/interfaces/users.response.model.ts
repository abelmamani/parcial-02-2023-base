import { Support } from "./support.model";
import { User } from "./user.model";

export interface UsersResponse{
    page: number,
    per_page: number,
    total_pages: number,
    data: User[],
    support: Support
    
}